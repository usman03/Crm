/*
SQLyog Enterprise - MySQL GUI v7.02 
MySQL - 5.5.5-10.1.9-MariaDB : Database - project_crm
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`project_crm` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `project_crm`;

/*Table structure for table `tbl_admin` */

DROP TABLE IF EXISTS `tbl_admin`;

CREATE TABLE `tbl_admin` (
  `pk_admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_email` varchar(110) DEFAULT NULL,
  `admin_password` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`pk_admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_admin` */

insert  into `tbl_admin`(`pk_admin_id`,`admin_email`,`admin_password`) values (1,'admin@gmail.com','d033e22ae348aeb5660fc2140aec35850c4da997');

/*Table structure for table `tbl_gym_detail` */

DROP TABLE IF EXISTS `tbl_gym_detail`;

CREATE TABLE `tbl_gym_detail` (
  `pk_gym_id` int(11) NOT NULL AUTO_INCREMENT,
  `gym_name` varchar(120) DEFAULT NULL,
  `gym_address` varchar(120) DEFAULT NULL,
  `gym_status` enum('Active','InActive') DEFAULT 'Active',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `pk_operator_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`pk_gym_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_gym_detail` */

insert  into `tbl_gym_detail`(`pk_gym_id`,`gym_name`,`gym_address`,`gym_status`,`date_created`,`date_modified`,`pk_operator_id`) values (2,'TestGym','Test Address, 201754123','Active','2018-05-02 02:23:08','2018-05-02 23:04:46',3),(3,'asdfasfd','asdf','Active','2018-05-02 23:08:43','2018-05-02 23:08:43',3);

/*Table structure for table `tbl_relation_user_with_service` */

DROP TABLE IF EXISTS `tbl_relation_user_with_service`;

CREATE TABLE `tbl_relation_user_with_service` (
  `user_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_relation_user_with_service` */

insert  into `tbl_relation_user_with_service`(`user_id`,`service_id`) values (32,11),(33,12),(50,11),(50,12),(51,11),(51,12),(51,13),(52,11),(52,12),(52,13),(53,12),(53,13),(54,12),(54,13);

/*Table structure for table `tbl_service` */

DROP TABLE IF EXISTS `tbl_service`;

CREATE TABLE `tbl_service` (
  `pk_service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_title` varchar(120) DEFAULT NULL,
  `service_price` int(11) DEFAULT NULL,
  `service_status` enum('Active','InActive') NOT NULL DEFAULT 'Active',
  `pk_operator_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`pk_service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_service` */

insert  into `tbl_service`(`pk_service_id`,`service_title`,`service_price`,`service_status`,`pk_operator_id`) values (11,'Main workout area',10,'Active',3),(12,'Trainer',25,'Active',3),(13,'Test Service',20,'Active',NULL);

/*Table structure for table `tbl_sessions` */

DROP TABLE IF EXISTS `tbl_sessions`;

CREATE TABLE `tbl_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tbl_sessions` */

/*Table structure for table `tbl_site_operator` */

DROP TABLE IF EXISTS `tbl_site_operator`;

CREATE TABLE `tbl_site_operator` (
  `pk_operator_id` int(11) NOT NULL AUTO_INCREMENT,
  `operator_name` varchar(120) DEFAULT NULL,
  `operator_email_address` varchar(120) DEFAULT NULL,
  `operator_phone_no` varchar(120) DEFAULT NULL,
  `operator_password` varchar(120) DEFAULT NULL,
  `operator_status` enum('Active','InActive') DEFAULT 'Active',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`pk_operator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_site_operator` */

insert  into `tbl_site_operator`(`pk_operator_id`,`operator_name`,`operator_email_address`,`operator_phone_no`,`operator_password`,`operator_status`,`date_created`,`date_modified`) values (2,'dsafsdf','admin1@gmail.com','1656151544','d033e22ae348aeb5660fc2140aec35850c4da997','Active',NULL,NULL),(3,'test','admin@gmail.com','123','d033e22ae348aeb5660fc2140aec35850c4da997','Active',NULL,NULL);

/*Table structure for table `tbl_user_detail` */

DROP TABLE IF EXISTS `tbl_user_detail`;

CREATE TABLE `tbl_user_detail` (
  `pk_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_first_name` varchar(120) DEFAULT NULL,
  `user_last_name` varchar(120) DEFAULT NULL,
  `user_phoneno` varchar(120) DEFAULT NULL,
  `user_age` int(11) DEFAULT NULL,
  `user_weight` int(11) DEFAULT NULL,
  `user_height` varchar(120) DEFAULT NULL,
  `user_detail` varchar(120) DEFAULT NULL,
  `user_picture` varchar(120) DEFAULT NULL,
  `user_address` varchar(120) DEFAULT NULL,
  `user_city` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`pk_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user_detail` */

insert  into `tbl_user_detail`(`pk_user_id`,`user_first_name`,`user_last_name`,`user_phoneno`,`user_age`,`user_weight`,`user_height`,`user_detail`,`user_picture`,`user_address`,`user_city`) values (32,'Test_User','Test','033355454555',30,80,'5.10',NULL,'1524270814214815ada86de591fb.png','Test','Isb'),(33,'Test_User_2','user','54545454',35,80,'172',NULL,'1524271839159245ada8adf3113d.png','sdf','rrw'),(50,'sdfdf','','',12,0,'',NULL,'817248fb77fb5c2cef3f2c732ad257cb1fb9c5e4.png','',''),(51,'Test_User_2','','',14,0,'',NULL,'817248fb77fb5c2cef3f2c732ad257cb1fb9c5e4.png','',''),(52,'asdf','','',1213,0,'',NULL,'817248fb77fb5c2cef3f2c732ad257cb1fb9c5e4.png','',''),(53,'1`12112','','',1,0,'',NULL,'9f6822c4f2c18c61243c7b4a9676c3562f3f373f.jpg','',''),(54,'fasdf','','',12,0,'',NULL,'817248fb77fb5c2cef3f2c732ad257cb1fb9c5e4.png','','');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
