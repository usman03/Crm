-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Apr 17, 2018 at 09:19 PM
-- Server version: 5.5.42
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `project_crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_relation_user_with_service`
--

CREATE TABLE `tbl_relation_user_with_service` (
  `user_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_relation_user_with_service`
--

INSERT INTO `tbl_relation_user_with_service` (`user_id`, `service_id`) VALUES
(11, 4),
(12, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service`
--

CREATE TABLE `tbl_service` (
  `pk_service_id` int(11) NOT NULL,
  `service_title` varchar(120) DEFAULT NULL,
  `service_price` decimal(11,0) DEFAULT NULL,
  `service_status` enum('Active','InActive') DEFAULT 'Active'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_service`
--

INSERT INTO `tbl_service` (`pk_service_id`, `service_title`, `service_price`, `service_status`) VALUES
(2, 'Test', '2500', 'InActive'),
(4, 'Gym', '1500', 'Active'),
(6, 'Swimming', '600', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_address`
--

CREATE TABLE `tbl_user_address` (
  `id` int(11) NOT NULL,
  `user_name` varchar(120) DEFAULT NULL,
  `user_age` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_address`
--

INSERT INTO `tbl_user_address` (`id`, `user_name`, `user_age`, `date_created`, `date_modified`) VALUES
(11, 'User_1', 28, '2018-04-17 02:35:22', '2018-04-17 02:35:22'),
(12, 'User_2', 30, '2018-04-17 02:35:33', '2018-04-17 02:35:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_service`
--
ALTER TABLE `tbl_service`
  ADD PRIMARY KEY (`pk_service_id`);

--
-- Indexes for table `tbl_user_address`
--
ALTER TABLE `tbl_user_address`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_service`
--
ALTER TABLE `tbl_service`
  MODIFY `pk_service_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_user_address`
--
ALTER TABLE `tbl_user_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;