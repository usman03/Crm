<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Basecontroller extends CI_Controller {

    public $themeUrl;
    protected $alertMessages;
    protected $adminObj;
    protected $uploadPath;

    public function __construct() {
        parent::__construct();
        $this->setThemeUrl();
        $this->setAlertMessages();
        $this->load->library('session');
        $this->load->model('admin/Adminmodel');
        $this->adminObj = new Adminmodel;
        $this->uploadPath = APPPATH . '../public/uploads/';
        /*$exception_uri = array('index.php', 'admin', 'admin/logout');
        if (uri_string() !== '' && !in_array(uri_string(), $exception_uri) && $this->adminObj->loggedin() == FALSE) {
                redirect(base_url('index.php'));
        }*/
    }
    
    public function loadAdminLayout($data, $content_path) {
        if (empty($data)) {
            $data = array();
        }
        $template = 'admin/template/';
        $data['header'] = $this->load->view($template . 'header', $data, TRUE);
        $data['footer'] = $this->load->view($template . 'footer', $data, TRUE);
        $data['sidebar'] = $this->load->view($template . 'sidebar', $data, TRUE);
        $data['content'] = $this->load->view($content_path, $data, TRUE);
        $this->load->view($template . 'template', $data);
    }

    public function loadSiteLayout($content_path, $data = array()) {
        $template = 'site/template/';
        $data['header'] = $this->load->view($template . 'header', $data, TRUE);
        $data['footer'] = $this->load->view($template . 'footer', $data, TRUE);
        $data['content'] = $this->load->view($content_path, $data, TRUE);
        $this->load->view($template . 'template', $data);
    }

    public function loadLoginLayout($data, $content_path) {
        if (empty($data)) {
            $data = array();
        }
        $template = 'admin/template/';
        $data['login_content'] = $this->load->view($content_path, $data, TRUE);
        $this->load->view($template . 'template_login', $data);
    }
    
    public function loadOperatorLayout($data, $content_path) {
        if (empty($data)) {
            $data = array();
        }
        $template = 'operator/template/';
        $data['header'] = $this->load->view($template . 'header', $data, TRUE);
        $data['footer'] = $this->load->view($template . 'footer', $data, TRUE);
        $data['sidebar'] = $this->load->view($template . 'sidebar', $data, TRUE);
        $data['content'] = $this->load->view($content_path, $data, TRUE);
        $this->load->view($template . 'template', $data);
    }

    public function loadSiteBasicHeaderLayout($content_path, $data = array()) {
        $template = 'site/template/';
        $data['header'] = $this->load->view($template . 'header_basic', $data, TRUE);
        $data['footer'] = $this->load->view($template . 'footer', $data, TRUE);
        $data['content'] = $this->load->view($content_path, $data, TRUE);
        $this->load->view($template . 'template', $data);
    }
    
    protected function uploadFile($config, $file) {
        if (!is_dir($config['upload_path'])) {
            mkdir($config['upload_path'], 0775, TRUE);
        }
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload($file)) {
            $response['upload_status'] = TRUE;
            $response['file_data'] = $this->upload->data();
        } else {
            $response['upload_status'] = FALSE;
            $response['error_message'] = $this->upload->display_errors();
        }
        return $response;
    }

    public function loadLayoutnoHF($content_path, $data = array()) {
        $template = 'site/template/';
        $data['content'] = $this->load->view($content_path, $data, TRUE);
        $this->load->view($template . 'template_noHF', $data);
    }
    
    public function getOperatorGymLimit()
    {
        $value = 1;
        return $value;
    }

    private function setAlertMessages() {
        $this->alertMessages = array(
            'str_replace' => '{{message}}',
            'success' => '<div class="alert alert-success alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            {{message}}</div>',
            'info' => '<div class="alert alert-info alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            {{message}}</div>',
            'warning' => '<div class="alert alert-warning alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            {{message}}</div>',
            'danger' => '<div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            {{message}}</div>'
        );
//        echo 'all is well';
//        return $this->alertMessages;
    }

    private function setThemeUrl() {
        $this->themeUrl = base_url('public/');
        $this->themeUrlSite = base_url('public/front');
    }

}
