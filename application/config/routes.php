<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'index';

$route['services'] = 'operator/services';
$route['services/new'] = 'operator/services/saveservice';
$route['services/edit/(:any)'] = 'operator/services/saveservice/$1';
$route['services/new/(:any)'] = 'operator/services/saveservice/$1';


$route['users'] = 'admin/users';
$route['user/new'] = 'admin/users/saveuser';
$route['users'] = 'project_crm';

$route['user/edit/(:any)'] = 'admin/users/saveuser/$1';
$route['user/new/(:any)'] = 'admin/users/saveuser/$1';

$route['operator/new'] = 'operator/operator/saveoperator';
$route['operator/edit/(:any)'] = 'operator/operator/saveoperator/$1';
$route['operator/status/(:any)'] = 'operator/operator/status/$1';
$route['operator/new/(:any)'] = 'operator/operator/saveoperator/$1';
$route['operator'] = 'operator/operator/login';
$route['operator/index'] = 'operator/operator/index';
$route['operator/logout'] = 'operator/operator/logout';
$route['operator/delete/(:any)'] = 'operator/operator/delete/$1';

$route['operator/reports'] = 'operator/reporting/displayReports/'. date('d');
//$route['operator/edit/(:any)'] = 'operator/operator/saveoperator/$1';

$route['gym/new'] = 'operator/gymhandler/savegym';
$route['gym/status/(:any)'] = 'operator/gymhandler/status/$1';
$route['gym/edit/(:any)'] = 'operator/gymhandler/savegym/$1';
$route['gym/new/(:any)'] = 'operator/gymhandler/savegym/$1';
$route['gym/delete/(:any)'] = 'operator/gymhandler/delete/$1';
$route['gym/index'] = 'operator/gymhandler/index';

$route['services/getvalue/(:any)/(:any)'] = 'operator/services/getValue/$1/$2';

$route['reporting'] = 'admin/reporting/displayReports/'. date('d');
//$route['admin/reporting/displayReports/31'] = 'admin/reporting/displayReports/31';
// admin routes


// admin service route
//$route['services'] = 'admin/services';
//$route['services/(:any)/(:any)'] = 'admin/services/$1/$2';

//admin user route
$route['users'] = 'admin/users';
$route['admin'] = 'admin/admin';
$route['admin/logout'] = 'admin/admin/logout';

$route['default_controller'] = 'index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;