<?php $this->load->view('admin/partials/content_title'); ?>
<?php
$idSlug = isset($services->pk_service_id) ? '/' . $services->pk_service_id : '';
$attributes = array('id' => "product_form", 'data-parsley-validate' => "", 'class' => "form-horizontal form-label-left", 'novalidate' => "");
echo form_open_multipart(base_url('services/new' . $idSlug), $attributes);
$ser['service_title'] = isset($services->service_title) ? $services->service_title : '';
$ser['service_price'] = isset($services->service_price) ? $services->service_price : '';
?>
<?php if (!empty(validation_errors())): ?>
    <div class="alert alert-danger" id="">
        <?php echo validation_errors() ?>
    </div>
<?php endif; ?>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="service_title">Title <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="service_title" name="service_title" required="required" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $ser['service_title'] ?>">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="service_price">Price <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="service_price" name="service_price" required="required" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $ser['service_price'] ?>">
    </div>
</div>
<div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button type="reset" class="btn btn-primary">Cancel</button>
        <button type="submit" name="submit" value="submit" class="btn btn-success">Submit</button>
    </div>
</div>


<?php
echo form_close();
?>