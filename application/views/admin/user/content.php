<?php $this->load->view('admin/partials/content_title'); ?>
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>All Users</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php
                echo $this->session->flashdata('flashKey');
                if (!isset($userList) || !$userList) {
                    ?>
                    <h4 class="col-md-12 alert alert-warning">No data found</h4>
                    <?php
                } else {
                    ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>Age</th>
                                <th>Service</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($userList as $user) {
//                                $status = $service['service_status'] == 'Active' ? 'fa-eye' : 'fa-eye-slash';
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $user->pk_user_id ?></th>
                                    <td><?php echo $user->user_first_name ?></td>
                                    <td><?php echo $user->user_age ?></td>
                                    <td><?php echo $user->service_title ?></td>
                                    <td>
                                        <ul>
                                            <a href="<?php echo base_url('user/edit/' . $user->pk_user_id); ?>"><i class="fa fa-edit"></i></a>
                                            <a href="<?php echo base_url('admin/users/delete/' . $user->pk_user_id); ?>"><i class="fa fa-close"></i></a>
                                        </ul>

                                    </td>
                                </tr>
                                <?php
                            }
                            ?>

                        </tbody>
                    </table>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

</div>