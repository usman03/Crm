<?php $this->load->view('admin/partials/content_title'); ?>
<?php
$idSlug = isset($user->pk_user_id) ? '/' . $user->pk_user_id : '';
$attributes = array('id' => "product_form", 'data-parsley-validate' => "", 'class' => "form-horizontal form-label-left", 'novalidate' => "", 'upload' => 'upload/do_upload');
echo form_open_multipart(base_url('user/new' . $idSlug), $attributes);

$usr['user_first_name'] = isset($user->user_first_name) ? $user->user_first_name : '';
$usr['user_last_name'] = isset($user->user_last_name) ? $user->user_last_name : '';
$usr['user_phoneno'] = isset($user->user_phoneno) ? $user->user_phoneno : '';
$usr['user_age'] = isset($user->user_age) ? $user->user_age : '';
$usr['user_weight'] = isset($user->user_weight) ? $user->user_weight : '';
$usr['user_height'] = isset($user->user_height) ? $user->user_height : '';
$usr['user_address'] = isset($user->user_address) ? $user->user_address : '';
$usr['user_city'] = isset($user->user_city) ? $user->user_city : '';
$usr['user_image'] = isset($user->user_image) ? $user->user_image : '';
?>
<?php if (!empty(validation_errors())): ?>
    <div class="alert alert-danger" id="">
        <?php echo validation_errors() ?>
    </div>
<?php endif; ?>
<?php
if (!empty($this->session->flashdata('user_image_message'))): echo $this->session->flashdata('user_image_message');
endif;
?>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_first_name">First Name <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="user_first_name" name="user_first_name" required="required" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $usr['user_first_name'] ?>">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_last_name">Last Name <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="user_last_name" name="user_last_name" required="required" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $usr['user_last_name'] ?>">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_phoneno">Phone no.<span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="user_phoneno" name="user_phoneno" required="required" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $usr['user_phoneno'] ?>">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_age">Age <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="user_age" name="user_age" required="required" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $usr['user_age'] ?>">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_weight">Weight<span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="user_weight" name="user_weight" required="required" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $usr['user_weight'] ?>">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_height">Height<span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="user_height" name="user_height" required="required" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $usr['user_height'] ?>">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_address">Address<span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="user_address" name="user_address" required="required" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $usr['user_address'] ?>">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_city">City<span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="user_city" name="user_city" required="required" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $usr['user_city'] ?>">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_image">Upload Image<span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="user_image" name="user_image" required="required" class="form-control col-md-7 col-xs-12" type="file" value="<?php echo $usr['user_image'] ?>">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_age">Service <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select name="user_services[]" class="form-control" multiple>
<?php foreach ($services as $key => $service): ?>
                <option value="<?php echo $service->pk_service_id; ?>" <?php
                if (isset($user_services)):
                    foreach ($user_services as $val) {
                        if ($val['service_id'] == $service->pk_service_id) {
                            echo 'selected';
                        }
                    }
                endif;
                ?>><?php echo $service->service_title; ?></option>
            <?php endforeach; ?>
        </select>
        <!--$this->input->post('parent_id') : $competency->parent_id-->
    </div>
</div>
<div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button type="reset" class="btn btn-primary">Cancel</button>
        <button type="submit" name="submit" value="submit" class="btn btn-success">Submit</button>
    </div>
</div>


<?php
echo form_close();
?>