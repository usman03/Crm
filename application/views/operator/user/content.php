<?php $this->load->view('admin/partials/content_title'); ?>
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>All Operators</h2>
                <div class="clearfix"></div>
            </div>
            <a href="<?php echo base_url('operator/new'); ?>"><span class="glyphicon glyphicon-plus"> </span> Create New</a>
            <div class="x_content">
                <?php
//                echo "<pre>";
//                print_r($operatorList);
//                echo "</pre>";
                echo $this->session->flashdata('flashKey');
                echo $this->session->flashdata('operator_message');
                if (!isset($operatorList) || !$operatorList) {
                    ?>
                    <h4 class="col-md-12 alert alert-warning">No data found</h4>
                    <?php
                } else {
                    ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Operator Title</th>
                                <th>Email Address</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($operatorList as $operator) {
                                $status = $operator->operator_status == 'Active' ? 'fa-eye' : 'fa-eye-slash';
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $operator->pk_operator_id ?></th>
                                    <td><?php echo $operator->operator_name ?></td>
                                    <td><?php echo $operator->operator_email_address  ?></td>
                                    <td><?php echo $operator->operator_status ?></td>
                                    <td>
                                        <ul>
                                            <a href="<?php echo base_url('operator/edit/' . $operator->pk_operator_id); ?>"><i class="fa fa-edit"></i></a>
                                            <a href="<?php echo base_url('operator/status/' . $operator->pk_operator_id); ?>"><i class="fa <?php echo $status; ?>"></i></a>
                                            <a href="<?php echo base_url('operator/delete/' . $operator->pk_operator_id); ?>"><i class="fa fa-close"></i></a>
                                        </ul>

                                    </td>
                                </tr>
                                <?php
                            }
                            ?>

                        </tbody>
                    </table>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

</div>