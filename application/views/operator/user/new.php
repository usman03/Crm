<?php $this->load->view('admin/partials/content_title'); ?>
<?php
$idSlug = isset($operator->pk_operator_id) ? '/' . $operator->pk_operator_id : '';
$attributes = array('id' => "product_form", 'data-parsley-validate' => "", 'class' => "form-horizontal form-label-left", 'novalidate' => "");
echo form_open_multipart(base_url('operator/new' . $idSlug), $attributes);
//echo "<pre>";
//print_r($operator);
//echo "</pre>";
//exit();
$oper['operator_name'] = isset($operator->operator_name) ? $operator->operator_name : '';
$oper['operator_email_address'] = isset($operator->operator_email_address) ? $operator->operator_email_address : '';
$oper['operator_phone_no'] = isset($operator->operator_phone_no) ? $operator->operator_phone_no : '';

?>
<?php if (!empty(validation_errors())): ?>
    <div class="alert alert-danger" id="">
        <?php echo validation_errors() ?>
    </div>
<?php endif; ?>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="operator_name">Name <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="operator_title" name="operator_name" required="required" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $oper['operator_name'] ?>">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="operator_email_address">Email <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="operator_price" name="operator_email_address" required="required" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $oper['operator_email_address'] ?>">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="operator_password">Password <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="operator_price" name="operator_password" required="required" class="form-control col-md-7 col-xs-12" type="password" value="<?php  ?>">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="operator_phone_no">Phone no <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="operator_price" name="operator_phone_no" required="required" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $oper['operator_phone_no'] ?>">
    </div>
</div>

<div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button type="reset" class="btn btn-primary">Cancel</button>
        <button type="submit" name="submit" value="submit" class="btn btn-success">Submit</button>
    </div>
</div>


<?php
echo form_close();
?>