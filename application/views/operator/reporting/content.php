<?php $this->load->view('admin/partials/content_title'); ?>
<?php
$idSlug = isset($user->pk_user_id) ? '/' . $user->pk_user_id : '';
$attributes = array('id' => "product_form", 'data-parsley-validate' => "", 'class' => "form-horizontal form-label-left", 'novalidate' => "", 'upload' => 'upload/do_upload');
echo form_open_multipart();
?>
<div class="x_title">
    <div class="clearfix"></div>
</div>
<div class="form-group">


    <label class="control-label col-md-1" for="">Select Date:
    </label>
    <div class="col-md-2 col-sm-2 col-xs-12">
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
        <select name="user_services" class="form-control" onchange="show()" id="user_date">
            <?php foreach ($dates as $key => $date): ?>
                <option value="<?php echo $key; ?>"><?php echo $date; ?></option>
            <?php endforeach; ?>
        </select>
        <!--$this->input->post('parent_id') : $competency->parent_id-->
    </div>
</div>
<div class="x_title">
    <div class="clearfix"></div>
</div>


<?php
echo form_close();
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <?php
                echo $this->session->flashdata('services_message');
                if (!isset($userList) || !$userList) {
                    ?>
                    <h4 class="col-md-12 alert alert-warning">No data found</h4>
                    <?php
                } else {
                    ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Service</th>
                                <th>Services Price Per Day</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($userList as $key => $user) {
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $user['pk_operator_id'] ?></th>
                                    <td><?php echo $user['service_title'] ?></td>
                                    <td><?php echo $user['service_price'] ?></td>
                                    <td><?php echo $user['service_total_price'] ?></td>
                                </tr>
                                <?php
                            }
                            ?>

                        </tbody>
                    </table>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>

            var drop_down = document.getElementById("user_date");
            drop_down.onchange = function () {
                var drop_down_date = $('#user_date').find('option:selected').val();
                window.location.href = "<?php echo site_url('operator/reporting/displayReports/'); ?>" + drop_down_date;

//        var csfrData = {};
//        csfrData['<?php // echo $this->security->get_csrf_token_name();                      ?>']
//                = '<?php // echo $this->security->get_csrf_hash();                      ?>';
//
//        $.ajax({
//            type: "POST",
//            url: "reporting/displayReports/" + drop_down_date,
//            dataType: "json",
//            data: csfrData,
//            cache: false,
//            success: function (data) {
//                alert(data);
//
//            }
//        });

            };
</script>