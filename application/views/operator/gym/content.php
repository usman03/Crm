<?php $this->load->view('admin/partials/content_title'); ?>
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>All Gym</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php
//                echo "<pre>";
//                print_r($gymList);
//                echo "</pre>";
                echo $this->session->flashdata('flashKey');
                echo $this->session->flashdata('gym_message');
                if (!isset($gymList) || !$gymList) {
                    ?>
                    <h4 class="col-md-12 alert alert-warning">No data found</h4>
                    <?php
                } else {
                    ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Gym Title</th>
                                <th>Email Address</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($gymList as $gym) {
                                $status = $gym->gym_status == 'Active' ? 'fa-eye' : 'fa-eye-slash';
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $gym->pk_gym_id ?></th>
                                    <td><?php echo $gym->gym_name ?></td>
                                    <td><?php echo $gym->gym_address  ?></td>
                                    <td><?php echo $gym->gym_status ?></td>
                                    <td>
                                        <ul>
                                            <a href="<?php echo base_url('gym/edit/' . $gym->pk_gym_id); ?>"><i class="fa fa-edit"></i></a>
                                            <a href="<?php echo base_url('gym/status/' . $gym->pk_gym_id); ?>"><i class="fa <?php echo $status; ?>"></i></a>
                                            <a href="<?php echo base_url('gym/delete/' . $gym->pk_gym_id); ?>"><i class="fa fa-close"></i></a>
                                        </ul>

                                    </td>
                                </tr>
                                <?php
                            }
                            ?>

                        </tbody>
                    </table>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

</div>