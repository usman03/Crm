<?php $this->load->view('admin/partials/content_title'); ?>
<?php
$idSlug = isset($gym->pk_gym_id) ? '/' . $gym->pk_gym_id : '';
$attributes = array('id' => "product_form", 'data-parsley-validate' => "", 'class' => "form-horizontal form-label-left", 'novalidate' => "");
echo form_open_multipart(base_url('gym/new' . $idSlug), $attributes);
//echo "<pre>";
//print_r($gym);
//echo "</pre>";
//exit();
$data['gym_name'] = isset($gym->gym_name) ? $gym->gym_name : '';
$data['gym_address'] = isset($gym->gym_address) ? $gym->gym_address : '';


?>
<?php if (!empty(validation_errors())): ?>
    <div class="alert alert-danger" id="">
        <?php echo validation_errors() ?>
    </div>
<?php endif; ?>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gym_name">Name <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="gym_title" name="gym_name" required="required" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $data['gym_name'] ?>">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gym_address">Address <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input id="gym_price" name="gym_address" required="required" class="form-control col-md-7 col-xs-12" type="text" value="<?php echo $data['gym_address'] ?>">
    </div>
</div>

<div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button type="reset" class="btn btn-primary">Cancel</button>
        <button type="submit" name="submit" value="submit" class="btn btn-success">Submit</button>
    </div>
</div>


<?php
echo form_close();
?>