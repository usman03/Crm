<?php $this->load->view('admin/partials/content_title'); ?>
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>All Services</h2>
                <div class="clearfix"></div>
            </div>
            <a href="<?php echo base_url('services/new'); ?>"><span class="glyphicon glyphicon-plus"> </span> Create New</a>
            <div class="x_content">
                <?php
//                print_r($this->session->flashdata());
                echo $this->session->flashdata('flashKey');
                echo $this->session->flashdata('services_message');
                if (!isset($servicesList) || !$servicesList) {
                    ?>
                    <h4 class="col-md-12 alert alert-warning">No data found</h4>
                    <?php
                } else {
                    ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Service Title</th>
                                <th>Day Price</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($servicesList as $service) {
                                $status = $service->service_status == 'Active' ? 'fa-eye' : 'fa-eye-slash';
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $service->pk_service_id ?></th>
                                    <td><?php echo $service->service_title ?></td>
                                    <td><?php echo $service->service_price  ?></td>
                                    <td><?php echo $service->service_status ?></td>
                                    <td>
                                        <ul>
                                            <a href="<?php echo base_url('services/edit/' . $service->pk_service_id); ?>"><i class="fa fa-edit"></i></a>
                                            <a href="<?php echo base_url('operator/services/status/' . $service->pk_service_id); ?>"><i class="fa <?php echo $status; ?>"></i></a>
                                            <a href="<?php echo base_url('operator/services/delete/' . $service->pk_service_id); ?>"><i class="fa fa-close"></i></a>
                                        </ul>

                                    </td>
                                </tr>
                                <?php
                            }
                            ?>

                        </tbody>
                    </table>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

</div>