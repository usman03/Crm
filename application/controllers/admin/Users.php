<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users
 *
 * @author Administrator
 */
class Users extends Basecontroller {

    private $data;
    protected $servicesObj;
    protected $usersObj;

    public function __construct() {
        parent::__construct();
        $this->load->model('admin/Usersmodel');
        $this->load->model('admin/Servicesmodel');
        $this->load->library('upload');
        $this->load->library('session');

        $this->data = array(
            'page' => array('title' => 'User | Home'),
        );

        $this->usersObj = new Usersmodel;
        $this->servicesObj = new Servicesmodel;
    }

    public function index() {

        $this->data['userList'] = $this->usersObj->getUserList();
        $this->loadAdminLayout($this->data, 'admin/user/content');
    }

    public function saveuser($id = null) {
        if ($id) {
            $this->data['user'] = $this->usersObj->get($id);
            $this->data['user_services'] = $this->usersObj->getUserServices($id);
        }
        $rules = $this->usersObj->rules;
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == TRUE) {
            $data = $this->usersObj->array_from_post(array('user_first_name', 'user_age', 'user_last_name', 'user_phoneno', 'user_age', 'user_weight', 'user_height', 'user_address', 'user_city', 'user_picture'));
            $data['user_picture'] = empty($_FILES['user_image']['name']) ? '' : sha1(explode('.', $_FILES['user_image']['name'])[0]) . '.' . explode('.', $_FILES['user_image']['name'])[1];
            $result = $this->usersObj->save($data, $id);
            // upload image
            $lastInsertedID = $result['lastInsertedId'] == '' ? $id : $result['lastInsertedId'];
            empty($_FILES['user_image']['name']) ? '' : $this->uploadUserImage($lastInsertedID);
            // save services for users
            $this->deleteUserServices($lastInsertedID);
            if (isset($_POST) && !empty($_POST['user_services'])) {
                foreach ($_POST['user_services'] as $k => $v) {
                    $this->usersObj->saveUserService(array(
                        'user_id' => $lastInsertedID,
                        'service_id' => $v,
                    ));
                }
            }
            $message = str_replace($this->alertMessages['str_replace'], 'User Updated.', $this->alertMessages['success']);
            $this->session->set_flashdata('flashKey', $message);
            redirect(base_url('users'));
        }
        $this->data['services'] = $this->servicesObj->get();
        $this->loadAdminLayout($this->data, 'admin/user/new');
    }

    private function uploadUserImage($id) {
        $config['upload_path'] = $this->uploadPath . 'images/' . $id;
        $config['allowed_types'] = 'jpg|gif|png|jpeg';
        $config['max_size'] = 1024;
        $config['file_name'] = sha1(explode('.', $_FILES['user_image']['name'])[0]) . '.' . explode('.', $_FILES['user_image']['name'])[1];
        $result = $this->uploadFile($config, 'user_image');
        if ($result['upload_status']) {
            $this->session->set_flashdata('flashKey', str_replace($this->alertMessages['str_replace'], 'Image uploaded.', $this->alertMessages['success']));
        } else {
            $this->session->set_flashdata('flashKey', str_replace($this->alertMessages['str_replace'], $result['error_message'], $this->alertMessages['warning']));
            return redirect(base_url('admin/user/edit/' . $id));
        }
    }

    public function delete($id) {
        $this->usersObj->delete($id);
        $this->deleteUserServices($id);
        $message = str_replace($this->alertMessages['str_replace'], 'User Deleted.', $this->alertMessages['success']);
        $this->session->set_flashdata('flashKey', $message);
        redirect(base_url('users'));
    }

    public function deleteUserServices($id) {
        if (!$id) {
            return FALSE;
        }
        $this->db->where('user_id', $id);
        $this->db->delete('tbl_relation_user_with_service');
    }

}
