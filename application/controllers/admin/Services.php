<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Services
 *
 * @author Administrator
 */
class Services extends Basecontroller {

    private $data;
    protected $servicesObj;

    public function __construct() {
        parent::__construct();
        $this->data = array(
            'page' => array('title' => 'Services | Home'),
        );
        $this->load->model('admin/Servicesmodel');

        $this->servicesObj = new Servicesmodel;
    }

    public function index() {

        $this->data['servicesList'] = $this->servicesObj->get();
        $this->loadAdminLayout($this->data, 'admin/services/content');
    }

    public function saveservice($id = null) {
        if ($id) {
            $this->data['services'] = $this->servicesObj->get($id);
        }

        $rules = $this->servicesObj->rules;
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == TRUE) {
            $data = $this->servicesObj->array_from_post(array('service_title', 'service_price'));
            $this->servicesObj->save($data, $id);
            $message = str_replace($this->alertMessages['str_replace'], 'Service Updated.', $this->alertMessages['success']);
            $this->session->set_flashdata('flashKey', $message);

            redirect(base_url('admin/services'));
        } else {
            $this->session->set_flashdata(validation_errors());
        }
        $this->loadAdminLayout($this->data, 'admin/services/new');
    }

    public function status($id) {
        $this->servicesObj->changeStatus($id);
        $message = str_replace($this->alertMessages['str_replace'], 'Status update successful.', $this->alertMessages['success']);
        $this->session->set_flashdata('flashKey', $message);
        redirect(base_url('admin/services'));
    }

    public function delete($id) {
        $this->servicesObj->delete($id);
        $message = str_replace($this->alertMessages['str_replace'], 'Service deleted.', $this->alertMessages['success']);
        $this->session->set_flashdata('services_message', $message);

        redirect(base_url('admin/services'));
    }

}
