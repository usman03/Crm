<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Reporting
 *
 * @author Administrator
 */
class Reporting extends Basecontroller {
    
    protected $usersObj;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('admin/Servicesmodel');
        $this->load->model('admin/Usersmodel');
        $this->data = array(
            'page' => array('title' => 'Reporting | Home'),
        );
        $this->usersObj = new Usersmodel;
    }

    public function index() {
        $this->loadAdminLayout($this->data, 'admin/reporting/content');
    }

    public function displayReports($date_value) {

        $six_months = (strtotime(date("Y-m-31", strtotime("-1 month"))) - strtotime(date("Y-m-01", strtotime("-6 month")))) / (60 * 60 * 24);
        $tweleve_months = (strtotime(date('Y-m-31', strtotime("-1 month"))) - strtotime(date("Y-m-01", strtotime("-12 month")))) / (60 * 60 * 24);
        $this->data['dates'] = array(
            '' => '',
            date('d') => 'Current Month',
            date('d', strtotime('last day of previous month')) => 'Last Month',
            $six_months => 'Last 6 Month',
            $tweleve_months => 'Last 12 Month',
        );
        $this->data['userList'] = $this->usersObj->getUserList();
        $this->data['user_services_price'] = $this->usersObj->getUserServicePrice($date_value);

        $this->loadAdminLayout($this->data, 'admin/reporting/content');
    }

}
