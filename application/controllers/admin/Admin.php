<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin
 *
 * @author Administrator
 */
class Admin extends Basecontroller {

    private $data;

    public function __construct() {
        parent::__construct();
        $this->data = array(
            'page' => array('title' => 'Admin | Login'),
        );
    }

    public function index() {
        $this->loadLoginLayout($this->data, 'login/login');
    }

    public function login() {
        // Redirect the user if already login 
        $dashboard = 'users';
        $this->adminObj->loggedin() == FALSE || redirect($dashboard);
        //set form
        $rules = $this->adminObj->rules;
        $this->form_validation->set_rules($rules);
        // process form
        if ($this->form_validation->run() == TRUE) {
            if ($this->adminObj->login() == TRUE) {
                $message = str_replace($this->alertMessages['str_replace'], 'Login successfully!', $this->alertMessages['success']);
                $this->session->set_flashdata('flashKey', $message);
                redirect($dashboard);
            } else {
                $message = str_replace($this->alertMessages['str_replace'], 'Wrong Email or Password', $this->alertMessages['danger']);
                $this->session->set_flashdata('flashKey', $message);
                redirect('admin');
            }
        } else {
            $this->session->set_flashdata(validation_errors());
        }
    }

    public function logout() {
        session_destroy();
        $this->session->sess_destroy();
        return redirect(base_url('index.php'));
    }

}
