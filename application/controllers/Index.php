<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of index
 *
 * @author Usman Akram
 */
class Index extends Basecontroller {

    public $data;

    public function __construct() {
        parent::__construct();

        $this->data = array(
            'page' => array('title' => 'Templete | Home'),
            'flashKey' => '',
        );
    }

    public function index() {
        $dashboard = 'users';
        $this->adminObj->loggedin() == FALSE || redirect($dashboard);
        $this->loadLoginLayout($this->data, 'login/login');
    }

}
