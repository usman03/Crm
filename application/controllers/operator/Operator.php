<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Operator
 *
 * @author Administrator
 */
class Operator extends Basecontroller {

    private $data;
    protected $operatorObj;

    public function __construct() {
        parent::__construct();
        $this->load->model('operator/Operatormodel');
        $this->data = array(
            'page' => array('title' => 'Operator | Home'),
        );
        
        $this->operatorObj = new Operatormodel;
        
    }

    public function index() {
        $this->data['operatorList'] = $this->operatorObj->get();
        $this->loadOperatorLayout($this->data, 'operator/user/content');
    }

    public function saveoperator($id = null) {
        if ($id) {
            $this->data['operator'] = $this->operatorObj->get($id);
        }
        $rules = $this->operatorObj->rules;
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == TRUE) {
            $data = $this->operatorObj->array_from_post(array('operator_name', 'operator_email_address', 'operator_phone_no'));
            $data['operator_password'] = sha1($this->input->post('operator_password'));
            $this->operatorObj->save($data, $id);
            $message = str_replace($this->alertMessages['str_replace'], 'Operater Updated.', $this->alertMessages['success']);
            $this->session->set_flashdata('flashKey', $message);

            redirect(base_url('operator/index'));
        } else {
            $this->session->set_flashdata(validation_errors());
        }
        $this->loadOperatorLayout($this->data, 'operator/user/new');
    }

    public function login() {
        // Redirect the user if already login 
        $dashboard = 'operator/index';
//        $this->operatorObj->loggedin() == FALSE || redirect($dashboard);
        //set form
        $rules = $this->operatorObj->operator_login_rules;
        $this->form_validation->set_rules($rules);
        // process form
        if ($this->form_validation->run() == TRUE) {
            if ($this->operatorObj->login() == TRUE) {
                $message = str_replace($this->alertMessages['str_replace'], 'Login successfully!', $this->alertMessages['success']);
                $this->session->set_flashdata('flashKey', $message);
                redirect($dashboard);
            } else {
                $message = str_replace($this->alertMessages['str_replace'], 'Wrong Email or Password', $this->alertMessages['danger']);
                $this->session->set_flashdata('flashKey', $message);
                redirect('operator');
            }
        } else {
            $this->session->set_flashdata(validation_errors());
        }
        $this->loadLoginLayout($this->data, 'login_operator/login');
    }

    public function logout() {
        session_destroy();
        $this->session->sess_destroy();
        return redirect(base_url('operator'));
    }

    public function status($id) {

        if ($this->operatorObj->changeStatus($id) == TRUE) {
            $message = str_replace($this->alertMessages['str_replace'], 'Status update successful.', $this->alertMessages['success']);
            $this->session->set_flashdata('flashKey', $message);
            redirect(base_url('operator/index'));
        }
    }
    
    public function delete($id) {
        $this->operatorObj->delete($id);
        $message = str_replace($this->alertMessages['str_replace'], 'Operator deleted.', $this->alertMessages['success']);
        $this->session->set_flashdata('services_message', $message);

        redirect(base_url('operator/index'));
    }

}
