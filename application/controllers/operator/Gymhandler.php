<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gymhandler
 *
 * @author Administrator
 */
class Gymhandler extends Basecontroller {

    private $data;
    protected $gymObj;

    public function __construct() {
        parent::__construct();
        $this->data = array(
            'page' => array('title' => 'Gym | Home'),
        );
        $this->load->model('operator/Gymhandlermodel');

        $this->gymObj = new Gymhandlermodel;
    }

    public function index() {
        $this->data['gymList'] = $this->gymObj->get();
        $this->loadOperatorLayout($this->data, 'operator/gym/content');
    }

    public function savegym($id = null) {
        // check operator limit to add gym
        if (count($this->gymObj->getOperatorGymCount()) >= $this->getOperatorGymLimit()) {
            $message = str_replace($this->alertMessages['str_replace'], 'You are not authorized to add more Gym.', $this->alertMessages['danger']);
            $this->session->set_flashdata('flashKey', $message);
            redirect(base_url('gym/index'));
        }
        if ($id) {
            $this->data['gym'] = $this->gymObj->get($id);
        }
        $rules = $this->gymObj->rules;
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == TRUE) {
            $data = $this->gymObj->array_from_post(array('gym_name', 'gym_address'));
            $data['pk_operator_id'] = $this->session->userdata('id');
            $this->gymObj->save($data, $id);
            $message = str_replace($this->alertMessages['str_replace'], 'Gym Updated.', $this->alertMessages['success']);
            $this->session->set_flashdata('flashKey', $message);
            redirect(base_url('gym/index'));
        } else {
            $this->session->set_flashdata(validation_errors());
        }
        $this->loadOperatorLayout($this->data, 'operator/gym/new');
    }

    public function status($id) {

        if ($this->gymObj->changeStatus($id) == TRUE) {
            $message = str_replace($this->alertMessages['str_replace'], 'Status update successful.', $this->alertMessages['success']);
            $this->session->set_flashdata('flashKey', $message);
            redirect(base_url('gym/index'));
        }
    }

    public function delete($id) {
        $this->gymObj->delete($id);
        $message = str_replace($this->alertMessages['str_replace'], 'Operator deleted.', $this->alertMessages['success']);
        $this->session->set_flashdata('services_message', $message);

        redirect(base_url('gym/index'));
    }

}
