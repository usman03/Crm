<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Reporting
 *
 * @author Administrator
 */
class Reporting extends Basecontroller {
    
    protected $operatorObj;
    protected $servicesObj;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('operator/Operatormodel');
        $this->load->model('operator/Servicesmodel');
        $this->data = array(
            'page' => array('title' => 'Reporting | Home'),
        );
        $this->operatorObj = new Operatormodel;
        $this->servicesObj = new Servicesmodel;
    }

    public function index() {
        $this->loadOperatorLayout($this->data, 'operator/reporting/content');
    }

    public function displayReports($date_value) {

        $six_months = (strtotime(date("Y-m-31", strtotime("-1 month"))) - strtotime(date("Y-m-01", strtotime("-6 month")))) / (60 * 60 * 24);
        $tweleve_months = (strtotime(date('Y-m-31', strtotime("-1 month"))) - strtotime(date("Y-m-01", strtotime("-12 month")))) / (60 * 60 * 24);
        $this->data['dates'] = array(
            '' => '',
            date('d') => 'Current Month',
            date('d', strtotime('last day of previous month')) => 'Last Month',
            $six_months => 'Last 6 Month',
            $tweleve_months => 'Last 12 Month',
        );
        $this->data['userList'] = $this->servicesObj->getOperatorWithServicePrice($date_value);
        $this->loadOperatorLayout($this->data, 'operator/reporting/content');
    }

}
