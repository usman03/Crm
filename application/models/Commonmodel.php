<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Commonmodel
 *
 * @author Usman AKram
 */
class Commonmodel extends CI_Model {

    protected $_table_name = '';
    protected $_primary_key = '';
    public $_order_by = '';
    protected $_timestamps = FALSE;
    public $rules = array();

    public function __construct() {
        parent::__construct();
    }

    public function save($data, $id = NULL) {

        //set timestamps
        if ($this->_timestamps === TRUE) {
            $now = date('Y-m-d H:i:s');
            if ($id) {
                $data['date_modified'] = $now;
            } else {
                $data['date_created'] = $now;
                $data['date_modified'] = $now;
            }
        }
        // insert
        if ($id === Null) {
            $this->db->set($data);
            $this->db->insert($this->_table_name, $data);
            $lastinserted_id = $this->db->insert_id();
            $result = array('status' => TRUE, 'lastInsertedId' => $lastinserted_id);
            return $result;
        } else {
            $this->db->set($data);
            $this->db->where($this->_primary_key, $id);
            $this->db->update($this->_table_name);
        }
    }

    public function array_from_post($fields) {
        $data = array();
        foreach ($fields as $field) {

            $data[$field] = $this->input->post($field);
        }
        return $data;
    }

    public function get($id = Null, $single = FALSE, $array = FALSE) {
        if ($id != Null) {
            $this->db->where($this->_table_name . '.' . $this->_primary_key, $id);
            $method = 'row';
        } elseif ($single == TRUE) {
            $method = 'row';
        } elseif ($array == TRUE) {
            $method = 'result_array';
        } else {
            $method = 'result';
        }
        if (!count($this->db->order_by($this->_order_by))) {
            $this->db->order_by($this->_order_by);
        }
        $result = $this->db->get($this->_table_name)->$method();

        return $result;
    }

    public function get_by($where, $single = FALSE) {
        $this->db->where($where);
        return $this->get(NULL, $single);
    }

    public function delete($id) {
        if (!$id) {
            return FALSE;
        }
        $this->db->where($this->_primary_key, $id);
        $this->db->limit(1);
        $this->db->delete($this->_table_name);
    }

}
