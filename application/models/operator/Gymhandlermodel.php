<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gymhandlermodel
 *
 * @author Administrator
 */
class Gymhandlermodel extends Commonmodel {

    protected $_table_name = 'tbl_gym_detail';
    protected $_primary_key = 'pk_gym_id';
    protected $_timestamps = TRUE;
    public $rules = array(
        'gym_name' => array(
            'field' => 'gym_name',
            'label' => 'Name',
            'rules' => 'trim|required'
        ),
        'gym_address' => array(
            'field' => 'gym_address',
            'label' => 'Address',
            'rules' => 'trim|required'
        ),
    );

    public function __construct() {
        parent::__construct();
    }

    public function getOperatorGymCount($id = null, $sigle = FALSE) {
        $this->db->where('pk_operator_id=', $this->session->id);
        return parent::get();
    }

    public function changeStatus($id) {

        if ($id != NULL) {
            $query = $this->db->query("UPDATE .$this->_table_name
            SET gym_status = (SELECT CASE gym_status WHEN 'Active' THEN 'InActive' ELSE 'Active' END)
            WHERE pk_gym_id = $id");
            return TRUE;
        }
    }

}
