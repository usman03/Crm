<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Operatormodel
 *
 * @author Administrator
 */
class Operatormodel extends Commonmodel {

    protected $_table_name = 'tbl_site_operator';
    protected $_primary_key = 'pk_operator_id';
    protected $_timestamps = TRUE;
    public $rules = array(
        'operator_name' => array(
            'field' => 'operator_name',
            'label' => 'Name',
            'rules' => 'trim|required'
        ),
        'operator_email_address' => array(
            'field' => 'operator_email_address',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email'
        ),
        'operator_password' => array(
            'field' => 'operator_password',
            'label' => 'Password',
            'rules' => 'trim|required'),
    );
    public $operator_login_rules = array(
        'log' => array(
            'field' => 'log',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email'
        ),
        'pwd' => array(
            'field' => 'pwd',
            'label' => 'Password',
            'rules' => 'trim|required'),
    );

    public function __construct() {
        parent::__construct();
    }

    public function login() {
        $user = $this->get_by(array(
            'operator_email_address' => $this->input->post('log'),
            'operator_password' => sha1($this->input->post('pwd'))
                ), TRUE);
        if (count($user)) {
            $data = array(
                'email' => $user->operator_email_address,
                'id' => $user->pk_operator_id,
                'loggedin' => TRUE,
            );
            $this->session->set_userdata($data);
            return TRUE;
        }
        return FALSE;
    }

    public function logout() {
        $this->session->sess_destroy();
    }

    public function loggedin() {
        return (bool) $this->session->userdata('loggedin');
    }

    public function changeStatus($id) {
    
        if ($id != NULL) {
            $query = $this->db->query("UPDATE .$this->_table_name
            SET operator_status = (SELECT CASE operator_status WHEN 'Active' THEN 'InActive' ELSE 'Active' END)
            WHERE pk_operator_id = $id");
            return TRUE;
        }
    }
    


}
