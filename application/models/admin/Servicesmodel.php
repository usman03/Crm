<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Services_m
 *
 * @author Administrator
 */
class Servicesmodel extends Commonmodel {

    protected $_table_name = 'tbl_service';
    protected $_timestamps = FALSE;
    protected $_primary_key = 'pk_service_id';
    public $rules = array(
        'service_title' => array(
            'field' => 'service_title',
            'label' => 'Title',
            'rules' => 'trim|required|max_length[100]'
        ),
        'service_price' => array(
            'field' => 'service_price',
            'label' => 'Price',
            'rules' => 'trim|required|numeric|max_length[5]'
        ),
    );

    public function __construct() {
        parent::__construct();
    }

    public function changeStatus($id) {

        if ($id != NULL) {
            $query = $this->db->query("UPDATE tbl_service
            SET service_status = (SELECT CASE service_status WHEN 'Active' THEN 'InActive' ELSE 'Active' END)
            WHERE pk_service_id = $id");
            return TRUE;
        }
    }
}
