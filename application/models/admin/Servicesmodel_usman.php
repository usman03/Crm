<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Servicesmodel
 *
 * @author Administrator
 */
class Servicesmodel extends Commonmodel {

    public function __construct() {
        parent::__construct();
    }

    public function getAllServices($status = NULL) {

        $query = is_null($status) ?
            "SELECT * FROM tbl_service " :
            "SELECT *  FROM tbl_service WHERE service_status  = ':status'";
        $statement = $this->prepQuery($query);
        $statement->bindParam(':status', $status, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        return count($result) > 0 ? $result : FALSE;
    }

    public function changeStatus($id) {
        $query = "UPDATE tbl_service
            SET service_status = (SELECT CASE service_status WHEN 'Active' THEN 'InActive' ELSE 'Active' END)
            WHERE pk_service_id = :id";
        $statement = $this->prepQuery($query);
        $statement->bindParam(':id', $id, PDO::PARAM_INT);
        return $statement->execute();
    }
    public function deleteservice($id) {
        $query = "DELETE FROM tbl_service WHERE pk_service_id = :id";
        $statement = $this->prepQuery($query);
        $statement->bindParam(':id', $id, PDO::PARAM_INT);
        return $statement->execute();
    }

    public function getServices($id) {
        $query = "SELECT * FROM tbl_service WHERE pk_service_id = :id";
        $statement = $this->prepQuery($query);
        $statement->bindParam(':id', $id, PDO::PARAM_STR);
        $statement->execute();
        return $statement->rowCount() > 0 ? $statement->fetch(PDO::FETCH_ASSOC) : FALSE;
    }

    

}
