<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Adminmodel
 *
 * @author Administrator
 */
class Adminmodel extends Commonmodel {

    protected $_table_name = 'tbl_admin';
    protected $_primary_key = 'pk_admin_id';
    public $rules = array(
        'log' => array(
            'field' => 'log',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email'
        ),
        'pwd' => array(
            'field' => 'pwd',
            'label' => 'Password',
            'rules' => 'trim|required'),
    );

    public function __construct() {
        parent::__construct();
    }

    public function login() {
        $user = $this->get_by(array(
            'admin_email' => $this->input->post('log'),
            'admin_password' => sha1($this->input->post('pwd'))
                ), TRUE);
        if (count($user)) {
            $data = array(
                'email' => $user->admin_email,
                'id' => $user->pk_admin_id,
                'loggedin' => TRUE,
            );
            $this->session->set_userdata($data);
            return TRUE;
        }
        return FALSE;
    }

    public function logout() {
        $this->session->sess_destroy();
    }

    public function loggedin() {
        return (bool) $this->session->userdata('loggedin');
    }

}
