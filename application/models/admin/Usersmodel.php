<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users_m
 *
 * @author Administrator
 */
class Usersmodel extends Commonmodel {

    protected $_table_name = 'tbl_user_detail';
    protected $_relation_table_name = 'tbl_relation_user_with_service';
    protected $_primary_key = 'pk_user_id';
    public $rules = array(
        'user_name' => array(
            'field' => 'user_first_name',
            'label' => 'Name',
            'rules' => 'trim|required|max_length[100]'
        ),
        'user_age' => array(
            'field' => 'user_age',
            'label' => 'Age',
            'rules' => 'trim|required|numeric|max_length[5]'
        ),
    );

    public function __construct() {
        parent::__construct();
    }

    public function saveUserService($data) {
        $this->_table_name = 'tbl_relation_user_with_service';
        return parent::save($data);
    }

    public function getUserList($id = null) {
        $this->db->select($this->_table_name . '.pk_user_id,user_first_name,user_age, GROUP_CONCAT(t3.service_title) as service_title, GROUP_CONCAT(t3.service_price) as service_price');
        $this->db->join('tbl_relation_user_with_service as t2', $this->_table_name . '.pk_user_id = t2.user_id', 'left');
        $this->db->join('tbl_service as t3', 't3.pk_service_id = t2.service_id', 'left');
        $this->db->group_by($this->_table_name . '.pk_user_id');
//        if (!empty($id)) {
//            $this->db->where($this->_table_name . '.pk_user_id=' . $id);
//        }
        $result = parent::get($id);
        return $result;
    }

    public function getUserServices($id) {
        $query = $this->db->query("SELECT t1.service_id, t2.service_title
                                                FROM tbl_relation_user_with_service as t1
                                                LEFT JOIN tbl_service as t2 on t1.service_id = t2.pk_service_id
                                                WHERE user_id = " . $id);
        $results = $query->result_array();
        return $results;
    }

    public function getUserServicePrice($days) {

        $users = $this->getUserList();
        $total = array();
        foreach ($users as $user) {
            $sumServices = array_sum(explode(',', $user->service_price));
            $total[] = $sumServices * $days;
        }
        return $total;
    }

}
